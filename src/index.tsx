import 'react-app-polyfill/ie11';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './app/ui/App';
import * as serviceWorker from './serviceWorker';
import {Provider} from "react-redux";
import appStateStore from "./app/store/AppStateStore";
import {BrowserRouter} from "react-router-dom";
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core";


const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
    props: {

        // Name of the component ⚛️
        MuiButtonBase: {
            // The properties to apply
            disableRipple: true, // No more ripple, on the whole application 💣!
        },
    },
});

ReactDOM.render(
    <Provider store={appStateStore}>
        <BrowserRouter basename={process.env.PUBLIC_URL}>
            <MuiThemeProvider theme={theme}>
                <App/>
            </MuiThemeProvider>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
