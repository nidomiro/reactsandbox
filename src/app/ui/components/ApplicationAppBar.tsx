import React from 'react';
import {WithStyles, withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {connect} from "react-redux";
import * as Redux from "redux";
import {openMainMenu} from "../../actions/MainMenuActions";
import AppStyles, {StyleType} from "../themes/ApplicationTheme"
import classNames from 'classnames';
import AppState from "../../store/AppState";

interface OwnProps {
    title: string
}

interface StateProps {
    menuOpen: boolean
}

interface DispatchProps {
    openSideMenu: () => void
}

type Props = StateProps & DispatchProps & WithStyles<keyof StyleType> & OwnProps;

class ApplicationAppBar extends React.PureComponent<Props> {

    static defaultProps: Partial<Props> = {
        // @ts-ignore
        classes: {},
        menuOpen: false,
        openSideMenu: () => {
        }
    };


    render() {
        const {
            classes,
            title,
            menuOpen,
            openSideMenu
        } = this.props;

        return (
            <AppBar
                position="absolute"
                className={classNames(classes.appBar, menuOpen && classes.appBarShift)}
            >
                <Toolbar disableGutters={!menuOpen} className={classes.toolbar}>

                    <IconButton
                        color="inherit"
                        aria-label="open menu"
                        onClick={openSideMenu}
                        className={classNames(
                            classes.menuButton,
                            menuOpen && classes.menuButtonHidden,
                        )}
                    >
                        <MenuIcon/>
                    </IconButton>

                    <Typography
                        component="h1"
                        variant="h6"
                        color="inherit"
                        noWrap
                        className={classes.pageTitle}
                    >
                        {title}
                    </Typography>

                    <Button color="inherit">Login</Button>

                </Toolbar>
            </AppBar>
        );
    }

}


function mapStateToProps(state: AppState): StateProps {
    return {
        menuOpen: state.mainMenu.menuOpen,
    }
}

function mapDispatchToProps(dispatch: Redux.Dispatch<any>): DispatchProps {
    return {
        openSideMenu: () => {
            dispatch(openMainMenu())
            dispatch({type: 'USER_FETCH_REQUESTED'})
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(AppStyles)(ApplicationAppBar)
)
