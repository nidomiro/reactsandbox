import * as React from "react";
import {CircularProgress} from "@material-ui/core";


const LoadingSpinner = () => {

    return (
        <CircularProgress/>
    )

};


export default LoadingSpinner;