import React from 'react';
import {Link} from "react-router-dom";
import {withStyles} from '@material-ui/core/styles';
import AppStyles from "../themes/ApplicationTheme"
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";


interface RoutedMenuItemViewProps {
    linkTitle: string;
    linkUrl: string;
    icon: any;
    currentUrl: string;
}

class RoutedMenuItem extends React.PureComponent<RoutedMenuItemViewProps> {

    public static defaultProps = {
        currentUrl: ""
    };

    public render(): JSX.Element {
        const {linkTitle, linkUrl, icon, currentUrl} = this.props;

        return (
            <ListItem button component={
                props =>
                    // @ts-ignore
                    <Link {...props} to={linkUrl}/>
            } selected={linkUrl == currentUrl}>
                <ListItemIcon>
                    {icon}
                </ListItemIcon>
                <ListItemText primary={linkTitle}/>
            </ListItem>
        );
    }

}

export default withStyles(AppStyles)(RoutedMenuItem);

