import React from "react";
import classNames from 'classnames';
import {WithStyles, withStyles} from '@material-ui/core/styles';
import Drawer from "@material-ui/core/Drawer";

import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";

import AppStyles, {StyleType} from "../themes/ApplicationTheme"
import {closeMainMenu} from "../../actions/MainMenuActions";
import {connect} from "react-redux";
import * as History from 'history';
import AppState from "../../store/AppState";
import MainMenuItems from "../../navigationDefinitions/MainMenuEntries";


interface OwnProps {
    location: History.Location<any>
}

interface StateProps {
    menuOpen: boolean
}

interface DispatchProps {
    closeSideMenu: () => void
}

type Props = StateProps & DispatchProps & WithStyles<keyof StyleType> & OwnProps;

class MainMenu extends React.PureComponent<Props> {

    render() {
        const {classes, location} = this.props;

        return (
            <Drawer
                variant="permanent"
                classes={{
                    paper: classNames(classes.drawerPaper, !this.props.menuOpen && classes.drawerPaperClose),
                }}
                open={this.props.menuOpen}
            >
                <div className={classes.toolbarIcon}>
                    <IconButton onClick={this.props.closeSideMenu}>
                        <ChevronLeftIcon/>
                    </IconButton>
                </div>
                <Divider/>
                <List>
                    <MainMenuItems currentUrl={location.pathname}/>
                </List>
            </Drawer>
        )

    }


}

function mapStateToProps(state: AppState): StateProps {
    return {
        menuOpen: state.mainMenu.menuOpen
    }
}

function mapDispatchToProps(dispatch: React.Dispatch<any>): DispatchProps {
    return {
        closeSideMenu: () => {
            dispatch(closeMainMenu())
        }
    }
}

export default connect<StateProps, DispatchProps, OwnProps, AppState>(mapStateToProps, mapDispatchToProps)(
    withStyles(AppStyles)(MainMenu)
);