import {List, Typography, withStyles, WithStyles} from "@material-ui/core";
import AppStyles, {StyleType} from "../themes/ApplicationTheme";
import * as React from "react";
import {oc} from "ts-optchain.macro";
import {postalAddressToString} from "../../domain/PostalAddressDto";
import ClubDto from "../../domain/ClubDto";
import OptionalListItem from "../components/OptionalListItem";

interface OwnProps {
    club: ClubDto | null | undefined
}

type Props = WithStyles<keyof StyleType> & OwnProps;

class DisplayClub extends React.PureComponent<Props> {

    render() {
        const {
            classes,
            club
        } = this.props;

        return (
            <div>
                <Typography variant={"h3"} gutterBottom>{oc(club).displayName()}</Typography>
                <List>
                    <OptionalListItem label={"Email"} value={oc(club).emailAddress()}/>

                    <OptionalListItem label={"Phone"} value={oc(club).phoneNumber()}/>

                    <OptionalListItem label={"Address"} value={oc(club).postalAddress()}
                                      transform={(value) => postalAddressToString(value)}/>
                </List>
            </div>
        );
    }
}

export default withStyles(AppStyles)(DisplayClub);