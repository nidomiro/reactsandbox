import * as React from "react";
import logo from '../../../logo.svg';
import AppStyles from "../themes/ApplicationTheme";
import {withStyles} from "@material-ui/core";

import './Home.css';


class Home extends React.PureComponent {
    render() {
        return (
            <div className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
            </div>
        );
    }
}

export default withStyles(AppStyles)(Home);
