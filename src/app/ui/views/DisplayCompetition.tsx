import * as React from "react";
import {List, Typography, withStyles, WithStyles} from "@material-ui/core";
import AppStyles, {StyleType} from "../themes/ApplicationTheme";
import CompetitionDto from "../../domain/CompetitionDto";
import {oc} from "ts-optchain.macro";
import {postalAddressToString} from "../../domain/PostalAddressDto";
import OptionalListItem from "../components/OptionalListItem";
import {RouteComponentProps, withRouter} from "react-router";

interface OwnProps {
    competition: CompetitionDto | null | undefined
}

type Props = RouteComponentProps<any> & WithStyles<keyof StyleType> & OwnProps;

class DisplayCompetition extends React.PureComponent<Props> {

    render() {
        let {
            classes,
            competition
        } = this.props;

        if (competition == null) {
            competition = {};
        }

        return (
            <div>
                <Typography variant={"h3"} gutterBottom>{oc(competition).name()}</Typography>
                <List>

                    <OptionalListItem label={"Date"} value={oc(competition).date()}
                                      transform={(value) => value.toLocaleString()}/>

                    <OptionalListItem label={"Organizer"} value={oc(competition).organizer.displayName()}
                                      onClick={
                                          (event: React.MouseEvent) => {
                                              this.props.history.push("/club/" + (oc(competition).organizer.identifier(-1)))
                                          }
                                      }/>

                    <OptionalListItem label={"Venue address"} value={oc(competition).venueAddress()}
                                      transform={(value) => postalAddressToString(value)}/>

                    <OptionalListItem label={"Max. participants"} value={oc(competition).maxParticipants()}/>

                    <OptionalListItem label={"Competition type"} value={oc(competition).competitionType.name()}/>

                    <OptionalListItem label={"Description"} value={oc(competition).description()}/>

                </List>
            </div>
        );
    }
}

export default withRouter(
    withStyles(AppStyles)(DisplayCompetition)
);