import * as React from 'react';
import SideMenu from "./components/MainMenu";
import AppStyles, {StyleType} from "./themes/ApplicationTheme";
import {WithStyles, withStyles} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import {Route, RouteComponentProps, Switch, withRouter} from "react-router-dom";
import {RoutedMenuItemData} from "../domain/RoutedMenuItemData";
import ApplicationAppBar from "./components/ApplicationAppBar";
import Routes from "../navigationDefinitions/Routes";


function routesFrom(array: RoutedMenuItemData[]) {
    return array.map((item) =>
        <Route key={item.linkUrl} exact path={item.linkUrl} component={item.targetComponent}/>
    )
}


type Props = RouteComponentProps<any> & WithStyles<keyof StyleType>

class App extends React.Component<Props> {
    render() {
        const {classes, location} = this.props;

        return (
            <div className={classes.root}>
                <CssBaseline/>
                <ApplicationAppBar title={"MyApp"}/>
                <SideMenu location={location}/>
                <main className={classes.content}>
                    <div className={classes.appBarSpacer}/>
                    <Switch>
                        <Routes/>
                    </Switch>
                </main>


            </div>
        );
    }
}

export default withRouter(
    withStyles(AppStyles)(App)
)
