import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import appStateStore from "../store/AppStateStore";
import {HashRouter} from "react-router-dom";

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <Provider store={appStateStore}>
            <HashRouter>
                <App/>
            </HashRouter>
        </Provider>
        , div);
    ReactDOM.unmountComponentAtNode(div);
});
