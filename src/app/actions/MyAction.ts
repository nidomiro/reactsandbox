import {Action} from "redux";


export interface MyAction extends Action<string> {
    discriminator: "MyAction"
}

export interface MyActionWithPayload<T = any> extends Action<string> {
    discriminator: "MyActionWithPayload",
    payload?: T
}