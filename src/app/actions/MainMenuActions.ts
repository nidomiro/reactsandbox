import {MainMenuActionTypes} from "../reducers/MainMenuReducer";
import {MyAction} from "./MyAction";


export function toggleMainMenuOpen(): MyAction {
    return <MyAction>{
        type: MainMenuActionTypes.TOGGLE_OPEN
    }
}

export function openMainMenu(): MyAction {
    return <MyAction>{
        type: MainMenuActionTypes.OPEN
    }
}

export function closeMainMenu(): MyAction {
    return <MyAction>{
        type: MainMenuActionTypes.CLOSE
    }
}

