import {oc} from "ts-optchain.macro";
import ClubDto from "../domain/ClubDto";


class ClubService {

    private dummyClub: ClubDto = {
        identifier: 0,
        name: "BSC Musterstadt",
        postalAddress: {
            streetWithNumber: "Musterstraße 1",
            postalCode: "12345",
            city: "Musterstadt"
        },
        displayName: "BSC Musterstadt",
        phoneNumber: "+491234546523",
        emailAddress: "mail@example.org"
    };


    public getClub = (id: number): ClubDto | undefined => {
        let comp: ClubDto = {...this.dummyClub};
        comp.name = oc(comp).name("");
        comp.identifier = id;
        return comp
    };


}

export default ClubService;