import CompetitionDto, {Visibility} from "../domain/CompetitionDto";
import {oc} from "ts-optchain.macro";
import ClubService from "./ClubService";


class CompetitionService {

    private dummyCompetition: CompetitionDto = {
        identifier: 0,
        name: "Test Competition",
        date: new Date(2019, 8, 25, 14, 30),
        organizer: {displayName: "BSC Musterstadt"},
        venueAddress: {
            streetWithNumber: "Musterstraße 1",
            postalCode: "12345",
            city: "Musterstadt"
        },
        maxParticipants: 2,
        competitionType: {
            name: "Fita"
        },
        description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        visibility: Visibility.PUBLIC
    };


    public getCompetition = (id: number): CompetitionDto | undefined => {
        let comp: CompetitionDto = {...this.dummyCompetition};
        comp.name = oc(comp).name("");
        comp.identifier = id;
        comp.organizer = new ClubService().getClub(id);
        return comp
    };


}

export default CompetitionService;