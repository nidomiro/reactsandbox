import {call, put, takeLatest} from 'redux-saga/effects'
import {MyAction} from "../actions/MyAction";
import {delay} from "./sagaUtil";
import axios from "axios";


function test(type: string) {
    return axios.get("http://google.de/?q=" + type);
}


// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchUser(action: MyAction) {
    try {
        const user = yield call(test, action.type);
        yield delay(2000);
        yield put({type: "USER_FETCH_SUCCEEDED", user: user});
    } catch (e) {
        yield put({type: "USER_FETCH_FAILED", message: e.message});
    }
}


function* mySaga() {
    yield takeLatest("USER_FETCH_REQUESTED", fetchUser);
}

export default mySaga;