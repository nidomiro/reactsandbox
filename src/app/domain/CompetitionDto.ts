import ClubDto from "./ClubDto";
import PostalAddressDto from "./PostalAddressDto";
import CompetitionTypeDto from "./CompetitionTypeDto";


export enum Visibility {
    PUBLIC = "PUBLIC",
    PRIVATE = "PRIVATE",
    SEMI_PRIVATE = "SEMI_PRIVATE"
}


export default interface CompetitionDto {
    identifier?: number,

    name?: string,

    date?: Date,

    organizer?: ClubDto,

    venueAddress?: PostalAddressDto,

    maxParticipants?: number,

    competitionType?: CompetitionTypeDto,

    description?: string,

    visibility?: Visibility
}

