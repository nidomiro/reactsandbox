export default interface PostalAddressDto {

    streetWithNumber: string,

    postalCode: string,

    city: string
}

export function postalAddressToString(address: PostalAddressDto) {
    return address.streetWithNumber + ", " + address.postalCode + " " + address.city;
}