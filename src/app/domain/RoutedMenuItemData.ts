import * as React from "react";

export interface RoutedMenuItemData {
    pageTitle: string;
    linkTitle: string;
    linkUrl: string;
    targetComponent: React.ComponentClass | React.FunctionComponent;
    icon: any;
}