export default interface CompetitionTypeDto {
    identifier?: number,

    name?: string,

    description?: string
}