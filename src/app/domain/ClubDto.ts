import PostalAddressDto from "./PostalAddressDto";


export default interface ClubDto {
    identifier?: number,

    name?: string,

    displayName?: string,

    emailAddress?: string,

    phoneNumber?: string,

    postalAddress?: PostalAddressDto
}