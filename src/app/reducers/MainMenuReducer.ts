import {MainMenuState} from "../domain/state/MainMenuState";
import {Reducer} from "redux";
import {MyAction} from "../actions/MyAction";

const initialState: MainMenuState = {
    menuOpen: false
};


export enum MainMenuActionTypes {
    TOGGLE_OPEN = '@@MainMenu/TOGGLE_OPEN',
    OPEN = '@@MainMenu/OPEN',
    CLOSE = '@@MainMenu/CLOSE'
}


export const mainMenuReducer: Reducer<MainMenuState, MyAction> =
    (state = initialState, action) => {
        let newState: MainMenuState = {...state};

        switch (action.type) {
            case MainMenuActionTypes.TOGGLE_OPEN:
                newState.menuOpen = !state.menuOpen;
                break;
            case MainMenuActionTypes.OPEN:
                newState.menuOpen = true;
                break;
            case MainMenuActionTypes.CLOSE:
                newState.menuOpen = false;
                break;
            default:
                break;
        }

        return newState;
    };
