import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import createSagaMiddleware from 'redux-saga'
import {mainMenuReducer} from "../reducers/MainMenuReducer";
import logger from "redux-logger";
import AppState from "./AppState";
import mySaga from "../sagas/dummySaga";

// @ts-ignore
const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const sagaMiddleware = createSagaMiddleware();

const middleWares = [sagaMiddleware, logger];

export default createStore(
    combineReducers<AppState>({
        mainMenu: mainMenuReducer
    }),
    storeEnhancers(
        applyMiddleware(...middleWares)
    )
);

sagaMiddleware.run(mySaga);
