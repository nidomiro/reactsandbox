import React from "react";
import * as Icons from '@material-ui/icons/';
import RoutedMenuItem from '../ui/components/RoutedMenuItem'

interface OwnProps {
    currentUrl: string
}


type Props = OwnProps;

const MainMenuItems = (props: Props) => (
    <div>
        <RoutedMenuItem linkTitle={"Dashboard"} linkUrl={"/"} icon={<Icons.Dashboard/>}
                        currentUrl={props.currentUrl}/>

        <RoutedMenuItem linkTitle={"Competition"} linkUrl={"/competition/0"} icon={<Icons.EditLocation/>}
                        currentUrl={props.currentUrl}/>
    </div>
)

export default MainMenuItems;