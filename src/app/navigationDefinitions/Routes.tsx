import {Route, RouteComponentProps, Switch} from "react-router";
import * as React from "react";
import {Typography} from "@material-ui/core";
import CompetitionService from "../service/CompetitionService";
import ClubService from "../service/ClubService";
import LoadingSpinner from "../ui/components/LoadingSpinner";


const Home = React.lazy(() => import("../ui/views/Home"));
const DisplayClub = React.lazy(() => import("../ui/views/DisplayClub"));
const DisplayCompetition = React.lazy(() => import("../ui/views/DisplayCompetition"));


function Routes() {
    return (
        <React.Suspense fallback={<LoadingSpinner/>}>
            <Switch>
                <Route exact path={"/"} render={() => <Home/>}/>

                <Route exact path={"/competition/:competitionId"}
                       component={DisplayCompetitionPropMapper}
                />

                <Route exact path={"/club/:clubId"}
                       render={(props: RouteComponentProps<any>) => <DisplayClubPropMapper {...props} />}/>

                <Route component={NoMatch}/>
            </Switch>
        </React.Suspense>
    )

}

const NoMatch = () => (
    <Typography variant={"h2"}>Nothing here (404)</Typography>
);


const DisplayCompetitionPropMapper = (props: RouteComponentProps<any>) => {

    let competitionService = new CompetitionService();
    let competition = competitionService.getCompetition(props.match.params.competitionId);

    return (
        <DisplayCompetition competition={competition}/>
    )
};

const DisplayClubPropMapper = (props: RouteComponentProps<any>) => {

    let clubService = new ClubService();
    let club = clubService.getClub(props.match.params.clubId);

    return (
        <DisplayClub club={club}/>
    )
};


export default Routes;